# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Problem Statement
Design a parking lot which can hold n cars of different color and different registration numbers.

Every car has been issued a ticket for a spot and the spot has been assigned based on the nearest to the entry point.

The system should also return some queries such as:

* Registration numbers of all cars of a particular Color.
* Ticket number in which a car with a given registration number is placed.
* Ticket numbers of all ticket where a car of a particular color is placed.


## Proposed Solution 

Automated parking lot ticket system created using React.js, React Hooks and Bootstrap


1) Generate Virtual Parking lot with number of paking spaces and filled spots.
2) Add new vehicle and assign closest available spot to the new entrying vehicle.
3) Generate ticket number for each parked vehical
4) Search parked car based on ticket number, color, registration number.

Build with React components to display the current status of the lot.
Used useContext to manage the current state of the system.


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

