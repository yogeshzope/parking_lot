
import { useNavigate } from "react-router-dom";
import CarParking from "./CarParking.jpg"

const Header =()=>{

    const navigate = useNavigate()
    const sectionStyle = {
        width: "100%",
        height: "100vh",
        // backgroundImage: `url(${CarParking})`,
        backgroundColor:"cornflowerblue",
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat'
      };

const mapperHandler=()=>{
    navigate('/parkingLot')
}

    return(
        <div style={sectionStyle} >
            <header className="">
            <h1 style={{paddingTop:"200px",backgroundColor:"cornflowerblue"}}>Welcome To Parking Lot System</h1>  
<button onClick={mapperHandler} className="btn btn-success">Go To Parking </button>
            </header>
        </div>
    )
}

export default Header