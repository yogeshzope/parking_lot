import { render, screen } from '@testing-library/react';
import App from './App';
import EntryForm from './Screens/EntryForm';

test('renders learn react link', () => {
  render(<EntryForm />);
  const linkElement = screen.getByLabelText("Total Parking Slots");
  expect(linkElement).toBeInTheDocument();
});
