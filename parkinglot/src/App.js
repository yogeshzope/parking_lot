import logo from "./logo.svg";
import "./App.css";
import {BrowserRouter as Router,Routes,Route, useNavigate, Navigate} from "react-router-dom"
import ParkingLotScrren from "./Screens/ParkingLot";
import Header from "./Components/Header";
import ContextStore from "./Screens/Store";
function App() {

  return (
    <div className="App">
       {/* <h1>Welcome To Parking Lot System</h1> */}
        {/* <Header/> */}
      {/* <button  onClick={parkingLotHandler}>Go To Page</button> */}
       <ContextStore>
       <Routes>
       <Route path="/" element={<Navigate to="/home" />} />
          <Route path="/home" element={<Header/>} />
            <Route path="/parkingLot" element={<ParkingLotScrren/>} />
          </Routes>
       </ContextStore>
          
    </div>
  );
}

export default App;
