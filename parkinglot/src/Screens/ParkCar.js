import { useContext, useEffect, useState } from "react";
import { AvailableSlotContext, InitialCarContext, Parking_dataContext, SlotAllocateContext, SlotContext } from "./Store";

import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

const AddCarScreen = () => {
  const [availableSlots, setAvailableSlots] = useContext(AvailableSlotContext);
  const [disable, setDisable] = useState(false);
  const [regNumber, setRegNumber] = useState("");
  const [colour, setColour] = useState("");
  const [parking_data, setParking_data] = useContext(Parking_dataContext);
  const [error, setError] = useState("");
  const [slot,setSlot] = useContext(SlotAllocateContext)
  const [showtable,setshowTable] = useState(false)

  useEffect(() => {
    if (availableSlots === 0) {
      setDisable(true);
    } else {
      setDisable(false);
    }
  }, [availableSlots]);

  const columns = ["Ticket No", "Registration Number", "Colour", "Slot"];
  const options = {
    search: true,
    filter: false,
    print: false,
    download: false,
    viewColumns: false,
    selectableRows: false,
    responsive: "stacked",
    onRowsDelete: (rowsDeleted) => {
       const idsToDelete = rowsDeleted.parking_data.map(d => parking_data[d.dataIndex].Registration_Number); // array of all ids to to be deleted
       console.log(idsToDelete);
    },
    fixedHeaderOptions: {
      xAxis: false,
      yAxis: true,
    },
   
  };

  let getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MUIDataTableBodyCell: {
          root: {
            fontSize: "14px",
          },
        },
        MUIDataTableHeadCell: {
          fixedHeader: {
            fontSize: "16px",
            backgroundColor: "#C0C0C0",
            fontWeight: "bold",
            textAlign: "center",
          },
        },
        MuiTypography: {
          h6: {
            fontSize: "1.5rem",
          },
        },
        MuiToolbar: {
          root: {
            backgroundColor: "#E0E0E0",
          },
        },
      },
    });


const generateRandom =(slots)=>{
       setSlot(slots+1)
       return slot
}
const randomStr= (len, arr) =>{
    let ticket = '';
    for (let i = len; i > 0; i--) {
        ticket +=
          arr[Math.floor(Math.random() * arr.length)];
    }
    return "T"+ticket;
}
    

  const AddHandler = (data) => {
    setError("");
    if( regNumber !== "")
    {
    if (regNumber.match(/^[A-Z]{2}-\d{2}-[A-Z]{2}-[1-9]\d{3}$/)) {
      if (colour !== "") {
        if (parking_data.some((n) => n.Registration_Number == regNumber)) {
          setError("Registration Number already exist!!");
        } else {
          if(colour.match(/[A-Za-z]+/))
          {
            const parkingdata = {
              Ticket_Number :randomStr(10,'124'), 
              Registration_Number: regNumber,
              Colour: colour,
              Slot_Allocate: generateRandom(slot),
            };

        setParking_data((prvState) => {
          return [parkingdata, ...prvState];
        });
        setshowTable(true)
        setAvailableSlots(availableSlots - 1);
        setRegNumber("");
        setColour("");
          }
          else
          {
            setError("Please Enter the Color in Alphabates only ");
          }
        }
      } else {
        setError("Please Enter the Color ");
      }
    } else {
      setError("Please enter Registration number in format \"MH-12-DD-1212\" ")
    }
    }
    else{
      setError("Please Enter the Registration Number");

    }
  };

// const deleteHandler=(de)=>{
//   parking_data.pop()
// }

  return (
    <div class="card mt-3">
      <div class="card-body">
        <div className="add-block">
          <div className="row">
            <div className="col-12 h4">Available space: {availableSlots}</div>
            <div className="error">{error}</div>
            <div className="row ml-4">
              <label className="col-5">
                <span className="col-6">Registration No<span style={{color:'red'}}>*</span> </span>
                <input
                  className="col-6"
                  type="text"
                  name="regis"
                  placeholder="MH-12-DD-2020"
                  onChange={(e) => {
                    setRegNumber(e.target.value);
                  }}
                  value={regNumber}
                  maxLength="13"
                />
              </label>
              <label className="col-4">
                <span className="col-4">Colour<span style={{color:'red'}}>*</span> </span>
                <input
                autoComplete="off"
                pattern={"/^[A-Za-z]+$/"}
                  className="col-7"
                  type="text"
                  name="colour"
                  onChange={(e) => {
                    setColour(e.target.value.toUpperCase());
                  }}
                  value={colour}
                />
              </label>
              <div className="col-3">
                <button
                  disabled={disable}
                  className="btn btn-sm btn-warning"
                  onClick={AddHandler}
                >
                  Park
                </button>
              </div>
            </div>
          </div>
          <br />
          <hr/>
          { showtable &&  <MuiThemeProvider theme={getMuiTheme()}>
            <MUIDataTable
              data={
                parking_data &&
                parking_data.map((x) => {
                  return [
                    x.Ticket_Number,
                    x.Registration_Number,
                    x.Colour,
                    x.Slot_Allocate,
                    // <button onClick={deleteHandler(x.Ticket_Number)} className="btn btn-danger">Exit</button>
                  ];
                })
              }
              title={"Parking Record"}
              columns={columns}
              options={options}
            ></MUIDataTable>
          </MuiThemeProvider>}
          {/* //Yogesh code for testing purpose */}
        </div>
      </div>
    </div>
  );
};

export default AddCarScreen;
