import React, { useState } from "react";

export const SlotContext = React.createContext();
export const AvailableSlotContext = React.createContext();
export const ShowContext = React.createContext();
export const Parking_dataContext = React.createContext();
export const SlotAllocateContext = React.createContext();
const ContextStore = (props) => {
  const [totalParkingSlot, setTotalParkingSlot] = useState('');
  const [availableSlots, setAvailableSlots] = useState(0);
  const[show,setShow] = useState(false)
  const [parking_data, setParking_data] = useState([]);
  const [slot,setSlot] = useState(1)

  return (
    <SlotContext.Provider value={[totalParkingSlot, setTotalParkingSlot]}>
        <AvailableSlotContext.Provider value={[availableSlots, setAvailableSlots]}>
            <ShowContext.Provider value={[show,setShow]}>
                <Parking_dataContext.Provider value={[parking_data, setParking_data]}>
                    <SlotAllocateContext.Provider value={[slot,setSlot]}>
                        {props.children}
                    </SlotAllocateContext.Provider>
                </Parking_dataContext.Provider>
            </ShowContext.Provider>
        </AvailableSlotContext.Provider>
    </SlotContext.Provider>
  );
};

export default ContextStore;
