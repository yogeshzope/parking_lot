import { useContext, useEffect, useState } from "react";
import { AvailableSlotContext, InitialCarContext, Parking_dataContext, ShowContext, SlotAllocateContext, SlotContext } from "./Store";

import Car from "../Screens/images/Car.jpg"
const EntryForm=()=>{
const [totalParkingSlot,setTotalParkingSlot] = useContext(SlotContext)
const [availableSlots, setAvailableSlots] = useContext(AvailableSlotContext);
const [error,setError] = useState('')
const [show,setShow] = useContext(ShowContext)
const [parking_data, setParking_data] = useContext(Parking_dataContext);
const [slot,setSlot] = useContext(SlotAllocateContext)

    const availableSlot =()=>{ 
      setAvailableSlots(totalParkingSlot)
    }


	const generateHandler = () => {
        setError('')
        if(totalParkingSlot!=''){
            console.log("Total Parking Slot : "+totalParkingSlot);
            if(totalParkingSlot<=30 && totalParkingSlot>=2){ 
                availableSlot()
                setShow(true)
                setTotalParkingSlot('')
                setParking_data([])
                setSlot(1)
            }
            else
            {
                setError('Total Parking Slots in the range of 2 to 30 ')
            }
            
        }
        else{
            setError('Enter total number of parking slots')
            
        }
	}

    return(
        <div class="card" >
				<div class="card-body">	
					<div className="entry-block">
						<div className="row" >
							<label className="col-4"><span className="col-6">Total Parking Slots<span style={{color:'red'}}>*</span></span><input className="col-6" type="text" id='totalSlots' name="totalSlots" onChange={(e)=>{setTotalParkingSlot(e.target.value)}}  value={totalParkingSlot} placeholder="Please enter the total number of slot you want to create"  /></label>
							<div className="col-3"><button className="btn btn-primary btn-sm" onClick={generateHandler} >Create Slots</button></div>
							<div className="col-3"><img src={Car} /></div>
                           
						</div>
						<div className="error ml-3 mt-2">{error}</div>
					</div>
				</div>
			</div>
    )
}


export default EntryForm
