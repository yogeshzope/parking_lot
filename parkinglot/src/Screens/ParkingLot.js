
import { useContext } from "react";
import "../ParkingLot.css"
import AddCarScreen from "./ParkCar";
import EntryForm from './EntryForm';
import Parking_lot from "../Screens/images/Parking_lot.jpg"
import { ShowContext } from "./Store";
import { useNavigate } from "react-router-dom";

const ParkingLotScrren = ()=>{
    const [show,setShow] = useContext(ShowContext)
const navigate = useNavigate()
    const sectionStyle = {
        width: "100%",
        height: "100vh",
        backgroundImage: `url(${Parking_lot})`,
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat'
      };
const homeHandler=()=>{
    navigate('/home')
}

    return(
        <>
           <div style={ sectionStyle }>			
				<div className="main container-fluid" >
					<div className="content container">
                        <header>
                        <button style={{float:'right'}} className="btn btn-primary" onClick={homeHandler}>Home</button>
                        </header>
                        <br/><br/>
						<EntryForm />
					{show && <AddCarScreen />}
					</div>
					
				</div>
			</div>
        </>
    )
} 


export default ParkingLotScrren